#!/bin/bash

sudo DEBIAN_FRONTEND=noninteractive add-apt-repository ppa:webupd8team/java -y
sudo DEBIAN_FRONTEND=noninteractive apt-get update
echo debconf shared/accepted-oracle-license-v1-1 select true | sudo debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | sudo debconf-set-selections
sudo DEBIAN_FRONTEND=noninteractive apt-get install oracle-java8-installer -y
export JAVA_HOME=/usr/lib/jvm/java-8-oracle
export PATH=$JAVA_HOME/bin:$PATH
echo 'export JAVA_HOME=/usr/lib/jvm/java-8-oracle' >> /etc/bash.bashrc
echo 'export PATH=$JAVA_HOME/bin:$PATH' >> /etc/bash.bashrc

cd /opt/
wget https://download.jboss.org/wildfly/14.0.1.Final/wildfly-14.0.1.Final.tar.gz
tar -vzxf wildfly-14.0.1.Final.tar.gz
mv wildfly-14.0.1.Final wildfly
rm wildfly-14.0.1.Final.tar.gz
groupadd wildfly
useradd -s /bin/bash -d /home/wildfly -m -g wildfly wildfly
echo 'wildfly ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
sudo cp /opt/wildfly/docs/contrib/scripts/init.d/wildfly-init-debian.sh /etc/init.d/wildfly
update-rc.d wildfly defaults
update-rc.d wildfly start 20 3 4 5
sudo cp /opt/wildfly/docs/contrib/scripts/init.d/wildfly.conf /etc/default/
cd /opt/wildfly/standalone/configuration/
mv standalone.xml standalone-bk.xml
wget https://gitlab.com/zardolucas2/wildfly-mysql/raw/master/standalone.xml
mv standalone.xml standalone-1.xml
export PRIVATE_IPV4=$(curl -s http://169.254.169.254/metadata/v1/interfaces/private/0/ipv4/address)
cat standalone-1.xml | sed "s|porta_publica|${PRIVATE_IPV4}|g" | sed "s|mysql_host|${MYSQL_HOST}|g" | sed "s|mysql_pwd|${MYSQL_PWD}|g" | sed "s|mysql_usr|${MYSQL_USR}|g" > standalone.xml
cd /opt/wildfly/modules/system/layers/base/com/
wget https://gitlab.com/zardolucas2/wildfly-mysql/raw/master/mysql.tar
tar -vxf mysql.tar
rm mysql.tar
cd /root
chown -R wildfly:wildfly /opt/wildfly
chown -h wildfly:wildfly /opt/wildfly
ln -s /opt/wildfly/standalone/log/server.log server.log
ln -s /opt/wildfly/standalone/configuration/standalone.xml standalone.xml
sudo service wildfly start