#!/bin/bash

function s3get {
	#params
	path="${1}"
	bucket=$(cut -d '/' -f 1 <<< "$path")
	key=$(cut -d '/' -f 2- <<< "$path")
	region="${2:-us-west-1}"
	#load creds
	access="$AWS_ACCESS_KEY"
	secret="$AWS_SECRET_KEY"
	#compute signature
	contentType="text/html; charset=UTF-8" 
	date="`date -u +'%a, %d %b %Y %H:%M:%S GMT'`"
	resource="/${bucket}/${key}"
	string="GET\n\n${contentType}\n\nx-amz-date:${date}\n${resource}"
	signature=`echo -en $string | openssl sha1 -hmac "${secret}" -binary | base64` 
	#get!
	curl -H "x-amz-date: ${date}" \
		-H "Content-Type: ${contentType}" \
		-H "Authorization: AWS ${access}:${signature}" \
		"https://sfo2.digitaloceanspaces.com${resource}"
}

s3get ai8-lms/deploy/lms.war > /tmp/lms.war